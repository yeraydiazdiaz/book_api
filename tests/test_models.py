import pytest
from sqlalchemy.exc import IntegrityError

from book_api.models import request


def test_request_can_be_created(session):
    assert session.query(request.Request).count() == 0
    req = request.Request(email="alice@example.com", title="Example Title")

    session.add(req)
    session.commit()

    assert session.query(request.Request).count() == 1
    req = session.query(request.Request).first()
    assert req.email == "alice@example.com"
    assert req.title == "Example Title"


def test_request_email_is_required(session):
    req = request.Request(email=None, title="Example Title")

    with pytest.raises(IntegrityError):
        session.add(req)
        session.commit()


def test_request_title_is_required(session):
    req = request.Request(email="alice@example.com", title=None)

    with pytest.raises(IntegrityError):
        session.add(req)
        session.commit()

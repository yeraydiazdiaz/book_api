from pathlib import Path
import pytest
from alembic import command
from alembic.config import Config
from sqlalchemy import create_engine, orm

from book_api.config import settings
from book_api.app import create_app
from book_api import db


alembic_cfg = Config(Path(__file__).parent / "../alembic.ini")


@pytest.fixture(scope="session")
def db_engine():
    no_db_dsn = settings.POSTGRES_DSN[: settings.POSTGRES_DSN.rfind("/")]
    engine = create_engine(no_db_dsn, isolation_level="AUTOCOMMIT")
    with engine.connect() as connection:
        connection.execute(f"DROP DATABASE IF EXISTS {settings.DATABASE_NAME}")
        connection.execute(f"CREATE DATABASE {settings.DATABASE_NAME}")

    engine = create_engine(settings.POSTGRES_DSN, isolation_level="AUTOCOMMIT")
    command.upgrade(alembic_cfg, "head")
    yield engine
    engine.dispose()

    engine = create_engine(no_db_dsn, isolation_level="AUTOCOMMIT")
    with engine.connect() as connection:
        connection.execute(f"DROP DATABASE {settings.DATABASE_NAME}")


@pytest.fixture(scope="function")
def session(db_engine):
    db.Session.configure(bind=db_engine)

    connection = db_engine.connect()
    transaction = connection.begin()
    session = db.Session()

    yield session

    transaction.rollback()
    transaction.close()
    connection.close()
    session.close()


@pytest.fixture()
def client(session):
    app = create_app()
    with app.test_client() as client:
        with app.app_context():
            yield client

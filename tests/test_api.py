from book_api.models import request


def test_get_request(session, client):
    req = request.Request(email="alice@example.com", title="Example Title")
    session.add(req)
    session.commit()
    req = session.query(request.Request).first()

    resp = client.get(f"/request/{req.id}")

    assert resp.status_code == 200
    assert resp.json["email"] == "alice@example.com"
    assert resp.json["title"] == "Example Title"
    assert resp.json["id"]
    assert resp.json["timestamp"]


def test_get_non_existing_request(session, client):
    resp = client.get(f"/request/999999")

    assert resp.status_code == 404


def test_post_request(session, client):
    params = {"email": "alice@example.com", "title": "Example Title"}
    resp = client.post(f"/request/", json=params)

    assert resp.status_code == 201
    assert resp.json["email"] == "alice@example.com"
    assert resp.json["title"] == "Example Title"
    assert resp.json["id"]
    assert resp.json["timestamp"]


def test_post_request_invalid_email_returns_400(session, client):
    params = {"email": "invalid@email", "title": "Example Title"}
    resp = client.post(f"/request/", json=params)

    assert resp.status_code == 400


def test_post_request_missing_email_returns_400(session, client):
    params = {"email": "alice@example.com"}
    resp = client.post(f"/request/", json=params)

    assert resp.status_code == 400


def test_post_request_missing_title_returns_400(session, client):
    params = {"title": "Example Title"}
    resp = client.post(f"/request/", json=params)

    assert resp.status_code == 400


def test_delete_non_existing_request(session, client):
    resp = client.delete(f"/request/999999")

    assert resp.status_code == 404


def test_delete_request(session, client):
    req = request.Request(email="alice@example.com", title="Example Title")
    session.add(req)
    session.commit()
    session.refresh(req)
    resp = client.delete(f"/request/{req.id}")

    assert resp.status_code == 204
    assert not resp.data

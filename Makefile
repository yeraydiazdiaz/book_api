.PHONY: build serve

db-drop:
	dropdb -h localhost -U postgres book_api

db-create:
	createdb -h localhost -U postgres book_api

build:
	docker-compose -f docker/docker-compose.yml build

serve:
	docker-compose -f docker/docker-compose.yml up

db-setup:
	FLASK_APP=book_api.app flask db_setup

from distutils.core import setup

from setuptools import find_packages

setup(
    name="book_api",
    version="0.1.0",
    url="https://bitbucket.com/yeraydiazdiaz/book-api",
    description="Book API",
    author="Yeray Diaz Diaz",
    maintainer="Yeray Diaz Diaz",
    license="proprietary",
    packages=find_packages(where="src", exclude=("tests",)),
    package_dir={"": "src"},
    include_package_data=True,
    entry_points={"console_scripts": ["rx-setup-db=book_api.cli:setup_db"]},
)

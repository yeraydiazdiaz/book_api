from flask import Blueprint, request, jsonify, abort
from marshmallow import ValidationError

from book_api.models.request import Request
from book_api.schemas.request import request_schema
from book_api.db import Session

request_blueprint = Blueprint("request_views", __name__)


@request_blueprint.route("/<int:id>", methods=["GET"])
def get(id):
    session = Session()
    req = session.query(Request).get(id)
    if req is None:
        abort(404)
    serialized_req = request_schema.dump(req)
    return jsonify(serialized_req)


@request_blueprint.route("/", methods=["POST"])
def post():
    session = Session()
    try:
        data = request.get_json()
        request_schema.load(data)
    except ValidationError:
        abort(400)

    req = Request(email=data["email"], title=data["title"])
    session.add(req)
    session.commit()
    session.refresh(req)
    serialized_req = request_schema.dump(req)
    return jsonify(serialized_req), 201


@request_blueprint.route("/<int:id>", methods=["DELETE"])
def delete(id):
    session = Session()
    req = session.query(Request).get(id)
    if req is None:
        abort(404)

    session.delete(req)
    session.commit()

    return "", 204

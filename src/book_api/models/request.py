from sqlalchemy import BigInteger, Column, DateTime, String, func
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Request(Base):
    __tablename__ = "request"

    id = Column(BigInteger, primary_key=True, autoincrement=True)
    email = Column(String, nullable=False)
    title = Column(String, nullable=False)
    timestamp = Column(DateTime(timezone=True), server_default=func.now())

    def __repr__(self):
        return f'<Request title="{self.title}" email="{self.email}">'

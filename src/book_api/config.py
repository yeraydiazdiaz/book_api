import environ


@environ.config(prefix="RX")
class Config:
    TESTING = environ.bool_var(False)

    POSTGRES_HOST = environ.var("localhost")
    POSTGRES_USER = environ.var("postgres")
    POSTGRES_DATABASE = environ.var("book_api")
    POSTGRES_PASSWORD = environ.var("postgres")

    @property
    def DATABASE_NAME(self):
        return (
            self.POSTGRES_DATABASE + "_test" if self.TESTING else self.POSTGRES_DATABASE
        )

    @property
    def POSTGRES_DSN(self):
        return (
            f"postgresql+psycopg2://{self.POSTGRES_USER}:{self.POSTGRES_PASSWORD}@"
            f"{self.POSTGRES_HOST}/{self.DATABASE_NAME}"
        )


settings = environ.to_config(Config)

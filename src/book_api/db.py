from sqlalchemy import create_engine, orm
from sqlalchemy.ext.declarative import declarative_base

from book_api.config import settings

Base = declarative_base()
engine = create_engine(settings.POSTGRES_DSN)
sessionmaker = orm.sessionmaker(bind=engine)
Session = orm.scoped_session(sessionmaker)

from flask import Flask

from book_api.views.request import request_blueprint
from book_api.config import settings


def create_app():
    app = Flask("book_api")
    app.config.from_object(settings)
    app.register_blueprint(request_blueprint, url_prefix="/request")
    return app

from pathlib import Path

from alembic import command
from alembic.config import Config
from sqlalchemy import create_engine

from book_api.config import settings
from book_api.app import create_app

app = create_app()


@app.cli.command("setup-db")
def setup_db():
    """Creates and runs alembic migrations on the database."""
    no_db_dsn = settings.POSTGRES_DSN[: settings.POSTGRES_DSN.rfind("/")]
    engine = create_engine(no_db_dsn, isolation_level="AUTOCOMMIT")
    try:
        with engine.connect() as connection:
            connection.execute(f"CREATE DATABASE {settings.DATABASE_NAME}")
    except Exception:
        pass

    alembic_cfg = Config(Path.cwd() / "./alembic.ini")
    engine = create_engine(settings.POSTGRES_DSN, isolation_level="AUTOCOMMIT")
    command.upgrade(alembic_cfg, "head")

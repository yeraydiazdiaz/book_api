from marshmallow import fields

from book_api.models.request import Request

from . import BaseSchema


class RequestSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = Request

    email = fields.Email(required=True)


request_schema = RequestSchema()

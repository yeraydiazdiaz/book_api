from marshmallow_sqlalchemy import ModelSchema

from book_api.db import Session


class BaseSchema(ModelSchema):
    class Meta:
        sqla_session = Session

# Book API

## Development

1. Create a virtual environment using Python 3.7
2. `pip install -r requirements/dev.txt` will install the Python dependencies
3. `make db-create` to create a new database using default credentials
4. `FLASK_APP=book_api.app flask run` will run the Flask development server

Alternatively use the docker make recipes provided:

1. `make build`
2. `make serve`

## Interacting with the API

Once a server is running locally or in Docker, we can create book requests by
sending JSON data to the `/request/` endpoint:

```
curl -H "Content-Type: application/json" -d '{"email": "alice@example.com", "title": "Example Title"}' http://localhost:5000/request/
```

We may retrieve book requests in the same way:

```
curl http://localhost:5000/request/<ID>
```

And delete book requests:

```
curl --request DELETE http://localhost:5000/request/<ID>
```
